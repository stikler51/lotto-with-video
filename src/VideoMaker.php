<?php

namespace App;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverDimension;
use FFMpeg\FFMpeg;

class VideoMaker
{
    protected $workDirectoryPath = '/var/www/lotto-with-video/video/';

    public function makeVideo(): bool
    {
        $temporaryFiles = [];

        // Запись видео
        $filename = $this->startCreatingVideoOnSelenium();
        $temporaryFiles[] = $filename;

        // получение видео от селениума
        $this->copyVideoFromSelenium($filename, $this->workDirectoryPath);

        // Укорачивание продолжительности
        $filename = $this->shortenVideoDuration($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Обрезание части экрана
        $filename = $this->croppingPartOfScreen($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Переименование видео
        $this->renameVideo($filename, $this->workDirectoryPath);

        // Удаление промежуточных файлов
        $this->deleteIntermediateFiles($temporaryFiles, $this->workDirectoryPath);

        return true;

    }

    protected function startCreatingVideoOnSelenium(): string
    {
        echo 'Start creating video on selenium...<br>';
        $filename = "44.mp4";
        //
        $host = 'http://localhost:4444/wd/hub';
        $capabilities = [
            WebDriverCapabilityType::BROWSER_NAME => 'chrome',
            'enableVideo' => true,
            'screenResolution' => '2560x1440x24',
            'videoName' => $filename,
            'videoFrameRate' => 65
        ];
        $webDriver = RemoteWebDriver::create($host, $capabilities, 1200000, 1200000);
        $webDriver->manage()->window()->setSize(new WebDriverDimension(2560, 1440));

        $webDriver->get("https://stikler51.github.io/lotto/");
        sleep(20);
        $webDriver->findElement(WebDriverBy::xpath('/html/body/button[1]'))->click();
        $webDriver->findElement(WebDriverBy::xpath('/html/body/button[2]'))->click();

        $itr = 220;
        for ($i = 0; $i < $itr; $i++) {
            $webDriver->findElement(WebDriverBy::cssSelector('body'));
            sleep(15);
        }

        $webDriver->quit();

        echo 'done<br>';
        return $filename;
    }

    protected function copyVideoFromSelenium(string $filename, string $workDirectoryPath): bool
    {
        echo 'Copy video from selenium...<br>';
        $url = 'http://localhost:4444/video/' . $filename;
        $path = $workDirectoryPath . $filename;
        file_put_contents($path, file_get_contents($url));
        echo 'done<br>';

        return true;
    }

    protected function shortenVideoDuration(string $filename, string $workDirectoryPath): string
    {
        $newFilename = 'cut.mp4';

        echo 'Shorten video duration...<br>';
        \shell_exec("ffmpeg -i $workDirectoryPath$filename -ss 00:00:12 -t 00:23:00 -async 1 $workDirectoryPath$newFilename");
        echo 'done<br>';

        return $newFilename;
    }

    protected function croppingPartOfScreen(string $filename, string $workDirectoryPath): string
    {
        $newFilename = 'crop.mp4';

        echo 'Cropping part of screen...<br>';
        \shell_exec("ffmpeg -y -hide_banner -i '$workDirectoryPath$filename' -filter:v 'crop=iw-0:ih-100,scale=2560:1340' -pix_fmt yuv420p $workDirectoryPath$newFilename");
        echo 'done<br>';

        return $newFilename;
    }

    protected function renameVideo(string $filename, string $workDirectoryPath): string
    {
        $newFilename = date('l jS \of F Y h:i:s A') . '.mp4';

        echo 'Rename video...<br>';
        copy($workDirectoryPath . $filename, $workDirectoryPath . $newFilename);
        echo 'done<br>';

        return $newFilename;
    }

    protected function deleteIntermediateFiles(array $temporaryFiles, string $workDirectoryPath): string
    {
        $newFilename = '';

        echo 'Delete intermediate files...<br>';
        foreach ($temporaryFiles as $file) {
            unlink($workDirectoryPath . $file);
        }
        echo 'done<br>';

        return $newFilename;
    }
}
