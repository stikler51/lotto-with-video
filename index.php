<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once dirname(__DIR__) . '/lotto-with-video/vendor/autoload.php';

if (isset($_GET['video'])) {
    if ($_GET['video'] === 'make') {
        $videoMaker = new \App\VideoMaker;
        $status = $videoMaker->makeVideo();

        if ($status) {
            echo 'complete';
        } else {
            echo 'fail';
        }
    }
} else {
    http_response_code(404);
    echo 'page not found';
}
